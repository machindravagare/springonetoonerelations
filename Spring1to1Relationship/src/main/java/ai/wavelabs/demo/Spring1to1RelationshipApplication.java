package ai.wavelabs.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Spring1to1RelationshipApplication  implements CommandLineRunner{
	@Autowired
    private UserRepository users;

    @Autowired
    private FreePageRepository freePages;

    public static void main(String[] args) {
        SpringApplication.run(Spring1to1RelationshipApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // Clean all data from tables
        this.freePages.deleteAllInBatch();
        this.users.deleteAllInBatch();

        // Create two users
        User user1 = new User("Machindra", "vagare", "Machindrav@email.com", "Machindra123");
        User user2 = new User("Sai", "ravi", "Saivivek@email.com", "sai123");

        // Create two free pages
        FreePage freePage1 = new FreePage("Machindra-project", "https://www.machindrasite.com");
        FreePage freePage2 = new FreePage("sai-project", "https://www.saivsite.com");

        // Set the free page to the respective user
        user1.setFreePage(freePage1);
        user2.setFreePage(freePage2);

        // Set the user for the respective page
        freePage1.setUser(user1);
        freePage2.setUser(user2);

        // Persist data to database
        this.users.save(user1);
        this.users.save(user2);

    }
}