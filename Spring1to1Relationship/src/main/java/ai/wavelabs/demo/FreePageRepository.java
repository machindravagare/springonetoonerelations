package ai.wavelabs.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FreePageRepository extends JpaRepository< FreePage,Long>{

}
